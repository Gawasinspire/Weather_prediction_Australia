# Weather_prediction_Australia
#Data analysis Project 

Comparing different classification Methods for given data

Given Data: weatherAUS from package: Rattle
            locationAUS from the same Package
            
Infos about Variables from the Australian Government Bureau of Meteorology 

Response variable (should) = RainTomorrow(binary Variable)
predictors (can be) = Date, Location, Longitude, Latitude

part 1: Data Visualisation
  # overall or station wise plot
  # Plots of a specific variable over time
  # variables on the map plot
  # Boxplots of predictor for any 2 selected classes( say Dates)
  # plots for correlation
    (ggplot2)
     
  
part 2: Data Preparation
  # Predictor Selection: ? What can be a good predictor
  # Training given data
  # Testing data
  
    
part 3: Comparison of classification Models generated from data.
  # comparison of different models based on test results.
  # Assessment models
  # Interpretation or understanding the model 
  # Comments for future